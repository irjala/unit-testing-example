package unittests.facebook;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	long start, end;

	@Before
    public void start() {
        start = System.currentTimeMillis();
    }
    
    // Something should be returned
    @Test
    public void resultHasContent()
    {
        assertNotNull(App.friendFetch("facebook.com/jonasirjala"));
    }
    
    // Return object should be a String array
    @Test
    public void resultFormatAndLength()
    {
    	if(App.friendFetch("facebook.com/jonasirjala").getClass() != String[].class)
    	{
    		assertTrue( false );
    	}
    	else if(App.friendFetch("facebook.com/jonasirjala").length < 1)
    	{
    		assertTrue( false );
    	}
    	else
    	{
    		assertTrue( true );
    	}
    }

    // Test profile has a friend called Mikael Testman, should exist in the result
    @Test
    public void findFriend()
    {
    	Boolean checker = false;
    	String[] friends = App.friendFetch("facebook.com/jonasirjala");
    	for (String i : friends) {
    		  if(i == "Mikael Testman") {
    			  checker = true;
    		  }
    		}
        assertTrue(checker);
    }
    
    @After
    public void end() {
    	
    	end = System.currentTimeMillis() - start;
        System.out.println(end);
    }
    
    @Test
    public void slow()
    {
    	if(end >= 1)
    	{
    		assertTrue( false );
    	}
    }
}
